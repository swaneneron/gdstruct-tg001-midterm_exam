#include <iostream>
#include <string>
#include "OrderedArray.h"
#include <time.h>
#include "UnorderedArray.h"

using namespace std;

void main()
{
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << "\nGenerated array: " << endl;
	cout << "Unordered: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";
	cout << "\nOrdered: ";
	for (int i = 0; i < ordered.getSize(); i++)
   	cout << ordered[i] << "   ";
	cout << endl;
	while (true) {
		int choice;
		cout << "================================" << endl;
		cout << "What would you like to do?" << endl;
		cout << "1) Remove element at index 2) Search for element 3) Expand and generate random values" << endl;
		cout << endl;
		cin >> choice;

		if (choice == 1) {

			cout << "Please input what position the element to DELETE is located: " << endl;
			int pos;
			cin >> pos;
			unordered.remove(pos);
			cout <<" Chosen number has successfully been deleted!" << endl;
			cout << "List of UPDATES Elements: " << endl;
			cout << "Unordered:" << endl;
			for (int i = 0; i < size-1; i++) {
				cout << unordered[i] << ", ";
			}
			cout << endl;
			cout <<"Ordered:" << endl;
			for (int i = 0; i < size - 1; i++) {
				cout << ordered[i] << ", ";
			}
			cout << endl;
			system("pause");
			system("cls");
		}
		if (choice == 2) {
			cout << "\n\nEnter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			 result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
			system("pause");
			system("cls");
		}
		if (choice == 3) {
			cout << "Enter how  many elements you'd like to have : " << endl;
			int size;
			cin >> size;
			for (int i = 0; i < size; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			cout << " Chosen number has successfully been added" << endl;
			cout << "List of UPDATES Elements: " << endl;
			cout << "\nGenerated array: " << endl;
			cout << "Unordered: ";
			
			for (int i = 0; i < size; i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered: ";
			for (int i = 0; i < size;i++)
				cout << ordered[i] << "   ";
			cout << endl;

			system("pause");
			system("cls");
		}
	}
	

	
}